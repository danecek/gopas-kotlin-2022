package gopas.kotlin.customers

import javafx.scene.control.TextField
import tornadofx.*

data class Customer(val name: String)

class CustApp : App(MainView::class)

class CustomerView : View("Customers") {
    val customers = mutableListOf(Customer("Tom")).asObservable()
    override val root =
        titledpane("Customers") {
            tableview(customers) {
                readonlyColumn("Name", Customer::name)
            }
        }

}

class AddCustomer : Fragment() {
    override val root =
        form {
            lateinit var ntf: TextField
            fieldset("Customer") {
                field("Name") {
                    ntf = textfield()
                }
            }
            buttonbar {
                button("OK") {
                    action {
                        find<CustomerView>().customers.add(Customer(ntf.text))
                        close()
                    }
                }//.enableWhen { ntf.textProperty() }
                button("Cancel") {
                    action {
                        close()
                    }

                }

            }
        }
}

class MainView : View("Customers") {
    override val root =
        borderpane {
            top {
                menubar {
                    menu("Customer") {
                        checkmenuitem("Add") {
                            action {
                                find<AddCustomer>().openModal()
                            }
                        }

                    }
                }
            }
            center<CustomerView>()
        }

}

fun main(args: Array<String>) {
    launch<CustApp>()
}