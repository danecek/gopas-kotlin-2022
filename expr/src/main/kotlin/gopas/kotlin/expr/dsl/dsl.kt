package gopas.kotlin.expr.dsl

import gopas.kotlin.expr.Bin
import gopas.kotlin.expr.Expr
import gopas.kotlin.expr.Num
import gopas.kotlin.expr.Oper
import java.util.*

fun Stack<Expr>.num(value: Int) : Expr {
    val n = Num(value)
    this.push(n);
    return n;
}

fun Stack<Expr>.bin(op: Oper, builder: Stack<Expr>.()->Unit): Expr {
    builder()
    val r = pop()
    val l = pop()
    val res = Bin(op, l,r)
    push(res)
    return res
}

fun main() {
    val e = Stack<Expr>().
       bin(Oper.MLT) {
           bin(Oper.PLS) {
               num(1)
               num(2)
               num(20)
           }
           num(3)
       }
    println("$e == ${e.value}")
}

