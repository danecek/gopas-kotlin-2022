package gopas.kotlin.expr.nullpatt

import gopas.kotlin.expr.Expr
import gopas.kotlin.expr.Num
import gopas.kotlin.expr.Oper
import gopas.kotlin.expr.Un
import gopas.kotlin.expr.patt.optim

interface Patt {
    fun match(e: Expr): Expr?
}

open class UnPatt(val ops: List<Oper>, val opPatt: Patt = UnivPatt) : Patt {
    override fun match(e: Expr): Un? =
        if (e is Un && e.op in ops && opPatt.match(e.operand) != null) e
        else null
}

object UnMnsMnsPatt : UnPatt(listOf(Oper.MNS), UnPatt(listOf(Oper.MNS)))

object UnivPatt : Patt {
    override fun match(e: Expr) = e
}

fun Expr.optim(): Expr =
    UnPatt(listOf(Oper.PLS)).match(this)?.operand?.optim() ?:
    (UnMnsMnsPatt.match(this)?.operand as? Un)?.operand?.optim() ?:
    this


fun main() {
    val ue = Un(Oper.PLS, Un(Oper.PLS, Num(1)))
    println("$ue == ${ue.optim()}")
    val unmnsmns = Un(Oper.MNS, Un(Oper.MNS, Num(1)))
    println("$unmnsmns == ${unmnsmns.optim()}")
}
