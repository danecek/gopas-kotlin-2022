package gopas.kotlin.expr

enum class Oper(val txt: String) {
    PLS("+"), MLT("*"), MNS("-");

    override fun toString() = txt
}

sealed abstract class Expr {
    abstract val value: Int
}

open class Num(override val value: Int) : Expr() {
    override fun toString() = "$value"
}

object Zero : Num(0)
object One : Num(1)

class Un(val op: Oper, val operand: Expr) : Expr() {
    override val value = when (op) {
        Oper.PLS -> operand.value
        Oper.MNS -> - operand.value
        else -> throw IllegalStateException();
    }

    override fun toString() = "$op $operand"

}

class Bin(val op: Oper, val left: Expr, val right: Expr) : Expr() {
    override val value = when (op) {
        Oper.PLS -> left.value + right.value
        Oper.MNS -> left.value - right.value
        Oper.MLT -> left.value * right.value
    }

    override fun toString() = "$left $op $right"

}

val Expr.postFix: String
    get() =
        when (this) {
            is Num -> "$value"
            is Bin -> "${left.postFix} ${right.postFix} $op"
            is Un -> "${operand.postFix} $op"
        }

fun Expr.eval() =
    when (this) {
        is Num -> value
        is Bin -> when (op) {
            Oper.PLS -> left.value + right.value
            Oper.MNS -> left.value - right.value
            Oper.MLT -> left.value * right.value
        }
        is Un -> when (op) {
            Oper.PLS -> operand.value
            Oper.MNS -> -operand.value
            else -> throw IllegalStateException()
        }
    }


fun main() {
    val e = (Bin(
        Oper.PLS,
        One,
        Num(3)
    ))
    println(e.postFix)
    println(e.eval())
    println("hodnota $e je ${e.value}")

}