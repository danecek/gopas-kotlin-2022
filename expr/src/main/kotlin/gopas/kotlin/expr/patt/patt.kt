package gopas.kotlin.expr.patt

import gopas.kotlin.expr.Expr
import gopas.kotlin.expr.Num
import gopas.kotlin.expr.Oper
import gopas.kotlin.expr.Un

interface Patt {
    fun match(e: Expr): Boolean
}

open class UnPatt(val ops: List<Oper>, val opPatt: Patt = UnivPatt) : Patt {
    override fun match(e: Expr): Boolean {
        return e is Un && e.op in ops && opPatt.match(e.operand)
    }
}

object UnMnsMnsPatt : UnPatt(listOf(Oper.MNS), UnPatt(listOf(Oper.MNS)))

object UnivPatt : Patt {
    override fun match(e: Expr) = true
}

fun Expr.optim(): Expr =
    when {
        UnMnsMnsPatt.match(this) -> ((this as Un).operand as Un).operand.optim()
        UnPatt(listOf(Oper.PLS)).match(this) -> (this as Un).operand.optim()
        UnivPatt.match(this) -> this
        else -> throw IllegalArgumentException()
    }

fun main() {
    val ue = Un(Oper.PLS, Un(Oper.PLS, Num(1)))
    println("$ue == ${ue.optim()}")
    val unmnsmns = Un(Oper.MNS, Un(Oper.MNS, Num(1)))
    println("$unmnsmns == ${unmnsmns.optim()}")
}
