package gopas.kotlin.expr.dslq

import gopas.kotlin.expr.Bin
import gopas.kotlin.expr.Expr
import gopas.kotlin.expr.Num
import gopas.kotlin.expr.Oper
import java.util.*
import kotlin.collections.ArrayDeque

fun Queue<Expr>.num(value: Int) : Expr {
    val n = Num(value)
    this.add(n);
    return n;
}

fun Queue<Expr>.bin(op: Oper, builder: Queue<Expr>.()->Unit): Expr {
    val q:  Queue<Expr> = LinkedList<Expr>()
    q.builder()
    check(q.size == 2)
    val res = Bin(op, q.poll(), q.poll())
    add(res)
    return res
}

fun main() {
    val e = LinkedList<Expr>().
       bin(Oper.MLT) {
           bin(Oper.PLS) {
               num(1)
               num(2)
               num(20)
           }
           num(3)
       }
    println("$e == ${e.value}")
}

