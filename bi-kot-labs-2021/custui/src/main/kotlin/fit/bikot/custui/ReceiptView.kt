package fit.bikot.custui

import fit.bikot.custdomain.Customer
import fit.bikot.custdomain.CustomerFacade
import fit.bikot.custdomain.Receipt
import tornadofx.*

class ReceiptView : View("Receipts") {
    val rcpts = mutableListOf<Receipt>().asObservable()

    override val root = titledpane("Receipts") {
        tableview(rcpts) {
            collapsibleProperty().set(false)
            readonlyColumn("ID", Receipt::id)
            readonlyColumn("Description", Receipt::dsc)
        }
    }

    fun refresh(c: Customer? = null) {
        if (c == null) {
            rcpts.clear()
            root.text = "Receipts"
        } else {
            rcpts.setAll(CustomerFacade.rcptsByCusts(c.id))
            root.text = c.name + "'s Receipts"
        }
    }
}