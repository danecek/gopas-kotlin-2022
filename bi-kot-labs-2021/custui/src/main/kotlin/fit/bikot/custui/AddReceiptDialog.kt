package fit.bikot.custui

import fit.bikot.custdomain.Customer
import fit.bikot.custdomain.CustomerFacade
import javafx.scene.control.TextField
import tornadofx.*

class AddReceiptDialog : Fragment("Add Receipt") {
    override val root = form {
        var nmtf: TextField by singleAssign()
        fieldset("Receipt Data") {
            field("Receipt Description:") {
                nmtf = textfield { }
            }
        }
        buttonbar {
            button("Cancel") {
                action {
                    close()
                }
            }
            button("OK") {
                action {
                    params["selectedcustomer"]?.apply {
                        CustomerFacade.createRcpt((this as Customer).id, nmtf.text)
                    }
                    find<CustomerView>().refresh()
                    close()
                }
            }.enableWhen {
                nmtf.textProperty().isNotBlank()
            }
        }
    }
}