package fit.bikot.custui

import fit.bikot.custdomain.Customer
import fit.bikot.custdomain.CustomerFacade
import tornadofx.*

class CustomerView : View("Customers") {

    val custs = CustomerFacade.allCusts.toMutableList().asObservable()
    override val root =
        titledpane("Customers") {
            tableview(custs) {
                collapsibleProperty().set(false)
                readonlyColumn("ID", Customer::id)
                readonlyColumn("Name", Customer::name)
            }.apply {
                onSelectionChange {
                    find<ReceiptView>().refresh(it)
                }
            }.apply {
                contextmenu {
                    item("Add Receipt") {
                        action {
                            find<AddReceiptDialog>(mapOf("selectedcustomer" to selectedItem)).openModal()
                        }
                    }
                }
            }
        }


    fun refresh() {
        custs.setAll(CustomerFacade.allCusts)
    }
}
