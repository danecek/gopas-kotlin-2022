package fit.bikot.custui

import tornadofx.*

class CustomerUI : App(InitalView::class)

class InitalView : View(title = "Customer Manager") {
    override val root = borderpane {
        top {
            menubar {
                menu("Customers") {
                    item("Add") {
                        action {
                            find<AddCustomerDialog>().openModal()
                        }

                    }
                }
            }
        }
        center<CustomerView>()
        right<ReceiptView>()
    }
}

fun main() {
    launch<CustomerUI>()
}