package fit.bikot.custui

import fit.bikot.custdomain.CustomerFacade
import javafx.scene.control.TextField
import tornadofx.*

class AddCustomerDialog : Fragment("Add Customer") {
    override val root = form {
        lateinit var nmtf: TextField// by singleAssign()
        fieldset("Customer Data") {
            field("Customer Name:") {
                nmtf = textfield("name")
            }
        }
        buttonbar {
            button("Cancel") {
                action {
                    close()
                }
            }
            button("OK") {
                action {
                    CustomerFacade.create(nmtf.text)
                    find<CustomerView>().refresh()
                    close()
                }
            }.enableWhen {
                nmtf.textProperty().isNotBlank()
            }
        }
    }
}