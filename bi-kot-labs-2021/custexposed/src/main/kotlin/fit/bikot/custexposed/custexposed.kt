package fit.bikot.custexposed

import fit.bikot.custdomain.Customer
import fit.bikot.custdomain.CustomerDAO
import fit.bikot.custdomain.Receipt
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

object CustomerTable : IntIdTable() {
    val name = varchar("name", 50)
}

object ReceiptTable : IntIdTable() {
    val dsc = varchar("name", 50)
    val custId = reference("cust_id", CustomerTable)
}

object CustomerDAOExposed : CustomerDAO {
    init {
        Database.connect(
            "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1",
            driver = "org.h2.Driver"
        )
        transaction {
            SchemaUtils.create(CustomerTable, ReceiptTable)
        }
    }

    override val allCusts: Collection<Customer>
        get() = transaction {
            val custId2rcpts: Map<Int, List<Receipt>> = allRcpts.groupBy { it.custId }
            CustomerTable.selectAll().map {
                val custId = it[CustomerTable.id].value
                Customer(custId, it[CustomerTable.name], custId2rcpts[custId] ?: mutableListOf())
            }
        }

    override val allRcpts: Collection<Receipt>
        get() = transaction {
            ReceiptTable.selectAll().map {
                Receipt(it[ReceiptTable.id].value, it[ReceiptTable.dsc], it[ReceiptTable.custId].value)
            }
        }

    override fun create(name: String): Int =
        transaction {
            CustomerTable.insertAndGetId {
                it[CustomerTable.name] = name
            }.value
        }

    override fun createRcpt(custId: Int, dsc: String): Int =
        transaction {
            ReceiptTable.insertAndGetId {
                it[ReceiptTable.dsc] = dsc
                it[ReceiptTable.custId] = custId
            }.value
        }

    override fun rcptsByCusts(custId: Int): Collection<Receipt> {
        TODO("Not yet implemented")
    }

}

fun main() {
    val tomId = CustomerDAOExposed.create("Tom")
    CustomerDAOExposed.createRcpt(tomId, "rcpt1 for Tom")
    CustomerDAOExposed.createRcpt(tomId, "rcpt2 for Tom")
    println(CustomerDAOExposed.allCusts)
}