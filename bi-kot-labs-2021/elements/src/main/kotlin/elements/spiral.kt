import java.io.Reader
import java.io.StringReader

interface Elem {
    val width: Int
        get() = content[0].length
    val height: Int
        get() = content.size
    val content: Array<String>
}

abstract class AElem : Elem {
    override fun toString() = content.joinToString(separator = "\n")
}

fun Elem.above(other: Elem): Elem {
    require(width == other.width) { "nesouhlasi sirka" }
    return BasicElem(content + other.content)
}

fun Elem.beside(other: Elem): Elem {
    require(height == other.height) { "nesouhlasi vyska" }
    return BasicElem(Array(height) { ind -> content[ind] + other.content[ind] })
}

fun Elem.widenR(w: Int): Elem {
    require(w >= width)
    return if (w == width) this
    else this.beside(EmptyElem(w - width, height))
}

fun Elem.widenL(w: Int): Elem {
    require(w >= width)
    return if (w == width) this
    else EmptyElem(w - width, height).beside(this)
}

fun Elem.heightenD(h: Int): Elem {
    require(h >= height)
    return if (h == height) this
    else this.above(EmptyElem(width, h - height))
}

fun Elem.heightenU(h: Int): Elem {
    require(h >= height)
    return if (h == height) this
    else EmptyElem(width, h - height).above(this)
}

open class BasicElem(final override val content: Array<String>) : AElem() {
    init {
        require(content.all { it.length == content[0].length }) { "neni obdelnikove" }
    }
}

open class CharElem(ch: Char, w: Int, h: Int) : BasicElem(Array(h) { ch.toString().repeat(w) })

class HStringElem(s: CharArray) : BasicElem(arrayOf(s.concatToString()))
class VStringElem(s: CharArray) : BasicElem(Array(s.size) { s[it].toString() })

class CharElem2(ch: Char, w: Int, h: Int) : AElem() {
    override val content: Array<String>

    init {
        val l: String = ch.toString().repeat(w)
        content = Array(h) { l }
    }
}

class EmptyElem(w: Int, h: Int) : CharElem(' ', w, h)

fun spiralRead(reader: Reader, n: Int = 2, e: Elem? = null): Elem {
    val passedElm = if (e != null) e
    else {
        val c: Int = reader.read()
        require(c != -1) { "empty reader" }
        CharElem(c.toChar(), 1, 1)
    }
    val cha = CharArray(n) { ' ' }
    val l: Int = reader.read(cha, 0, n)
    if (l == -1) return passedElm
    val passingElm = when (n % 4) {
        2 -> {
            passedElm.heightenD(n).beside(VStringElem(cha))
        }
        3 -> {
            passedElm.widenL(n).above(HStringElem(cha.reversedArray()))
        }
        0 -> {
            VStringElem(cha.reversedArray()).beside(passedElm.heightenU(n))
        }
        1 -> {
            HStringElem(cha).above(passedElm.widenR(n))
        }
        else -> error("")
    }
    return spiralRead(reader, n + 1, passingElm)
}

fun spiral(n: Int, ch: Char = '*'): Elem {
    require(n > 0) { "argument must be >0" }
    return if (n == 1)
        CharElem('*', 1, 1)
    else {
        val spiral = spiral(n - 1)
        return when (n % 4) {
            0 -> {
                CharElem(ch, 1, n).beside(spiral.heightenU(n))
            }
            1 -> {
                CharElem(ch, n, 1).above(spiral.widenR(n))
            }
            2 -> {
                spiral.heightenD(n).beside(CharElem(ch, 1, n))
            }
            3 -> {
                spiral.widenL(n).above(CharElem(ch, n, 1))
            }
            else -> error("")
        }
    }
}

fun main() {
    println(spiral(20))
    val li = ("Po celou dobu železniční přepravy bataliónu, " +
            "který měl sklízet válečnou slávu, " +
            "až projde pěšky od Laborce východní Haličí na front," +
            "vedly se ve vagónu, kde byl jednoroční dobrovolník a Švejk, " +
            "opět podivné řeči, víceméně velezrádného obsahu.").replace(' ', '.')
    println(spiralRead(StringReader(li)))
}


