package fit.bikot.custdomain

data class Customer(val id: Int, val name: String, val rcpts: List<Receipt> = mutableListOf<Receipt>())
data class Receipt(val id: Int, val dsc: String, val custId: Int)

interface CustomerDAO {
    fun create(name: String): Int
    fun createRcpt(custId: Int, dsc: String): Int
    val allCusts: Collection<Customer>
    fun rcptsByCusts(custId: Int): Collection<Receipt>
    val allRcpts: Collection<Receipt>
}

object CustomerDAOMem : CustomerDAO {

    val custs = mutableMapOf<Int, Customer>()
    val rcpts = mutableMapOf<Int, Receipt>()

    init {
        create("Tom")
    }

    override fun create(name: String): Int {
        val c = Customer(custs.maxOfOrNull { c -> c.key + 1 } ?: 1, name)
        custs[c.id] = c
        return c.id
    }

    override fun createRcpt(custId: Int, dsc: String): Int {
        val r = Receipt(rcpts.maxOfOrNull { c -> c.key + 1 } ?: 1, dsc, custId)
        rcpts[r.id] = r
        return r.id
    }

    override val allCusts: Collection<Customer>
        get() = custs.values
    override val allRcpts: Collection<Receipt>
        get() = rcpts.values

    override fun rcptsByCusts(custId: Int) =
        allRcpts.filter { r -> r.custId == custId }
}

object CustomerFacade : CustomerDAO by CustomerDAOMem