package fit.bikot.custdomain

import junit.framework.TestCase
import junit.framework.TestCase.assertEquals

class CustomerDAOMemTest : TestCase() {

    fun testAllCusts() {
        CustomerFacade.create("Bob")
        assertEquals(1, CustomerFacade.allCusts.filter { it.name == "Bob" }.size)
    }
}