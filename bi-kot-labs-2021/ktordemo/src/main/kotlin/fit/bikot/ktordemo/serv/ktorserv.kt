package fit.bikot.ktordemo.serv

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlinx.html.*

fun main() {
    embeddedServer(
        factory = Netty, port = 8080,
        module = Application::mainModule
    ).start(wait = true)
}

data class Customer(val id: Int, val name: String)

val customers: Map<Int, Customer> =
    listOf(Customer(1, "Tom"), Customer(2, "Bob")).associateBy(Customer::id)

fun Application.mainModule() {
    install(ContentNegotiation) {
        gson { }
    }
    routing {
        route("customers") {
            get {
                call.respond(
                    customers.values
                )
            }
            get("{n}") {
                val c = customers[call.parameters["n"]!!.toInt()]
                c?.let { call.respond(c) } ?:
                call.respond(HttpStatusCode.NotFound, "unknown customer")
            }
        }
        get("/") {
            call.respondHtml {
                head {
                    link {
                        rel = "stylesheet"
                        href = "https://www.w3schools.com/w3css/4/w3.css"
                    }
                    meta {
                        name = "viewport"
                        content = "width=device-width, initial-scale=1"
                    }
                }
                body {
                    div("w3-container") {
                        h1 {
                            +"Customers"
                        }
                        table("w3-table-all") {
                            customers.values.forEach {
                                tr {
                                    td {
                                        +it.id.toString()
                                    }
                                    td {
                                        +it.name
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


