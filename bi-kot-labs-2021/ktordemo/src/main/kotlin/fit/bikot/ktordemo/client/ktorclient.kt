package fit.bikot.ktordemo.client

import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking

val rootUrl: Url = URLBuilder(
    protocol = URLProtocol.HTTP,
    // host = "127.0.0.1",
    host = "localhost",
    port = 8080
).build()

data class ClientCustomer(val id: Int, val name: String)

val client: HttpClient = HttpClient {
    install(JsonFeature) {
        serializer = GsonSerializer {
        }
    }
}

suspend fun getCust(n: Int): ClientCustomer {
    return client.get(rootUrl) {
        url.path("customers/$n")
    }
}

suspend fun customers(): List<ClientCustomer> {
    return client.get(rootUrl) {
        url.path("customers")
    }
}

fun main() {
    runBlocking {
        println(customers())
        println(getCust(2))
        println(getCust(4))
    }
}

