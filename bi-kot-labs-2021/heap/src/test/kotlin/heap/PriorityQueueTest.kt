package heap

import kotlin.test.Test
import kotlin.test.assertTrue

class PriorityQueueTest {
    @Test
    fun tetsEmpty() {
        val pq = PriorityQueue()
        assertTrue(pq.isEmpty())
    }
}