import heap.PriorityQueue
import heap.heapSort

fun main() {
    val pq0 = PriorityQueue()
    pq0.add(1.0)
    println(pq0)
    pq0.add(3.0)
    println(pq0)
    pq0.add(2.0)
    println(pq0)
    println("xxxx")
    val a: Array<Double> = arrayOf(1.0, 30.0, 10.0, 2.0, 33.9, 1.9, 1.9)
    val pq = PriorityQueue(*a.toDoubleArray())
    println(pq.poll())
    pq.add(11111.0)
    println(pq.poll())
    pq.add(111.0)
    pq.add(11.0)
    println(pq.poll())
    while (!pq.isEmpty()) {
        println(pq.poll())
    }
    a.heapSort()
    println(a.joinToString())

}
