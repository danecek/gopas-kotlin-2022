package heap

import java.util.*

class PriorityQueue(vararg elems: Double, maxSize: Int = 1000) : Queue<Double> {

    private val backupArray = Array(maxOf(maxSize, elems.size)) {
        if (it <= elems.lastIndex) elems[it] else 0.0
    }
    private var _size = elems.size

    init {
        buildHeap(backupArray, 0, lastElemInd)
    }

    override fun add(element: Double?): Boolean {
        require(element != null) { "null element" }
        backupArray[size] = element
        _size++
        var downInd = lastElemInd
        var topInd = parentInd(downInd)
        while(backupArray[topInd] < backupArray[downInd]){
            swap(backupArray, downInd, topInd)
            downInd = topInd
            topInd = parentInd(downInd)
        }
/*        while (topInd > 0) {
            topInd = parentInd(topInd)
            repairTop(backupArray, topInd, lastElemInd)
        }*/
        return true
    }

    override fun poll(): Double? {
        if (isEmpty()) return null
        val res = backupArray[0]
        swap(backupArray, 0, lastElemInd)
        --_size
        repairTop(backupArray, 0, lastElemInd)
        return res
    }

    override val size: Int
        get() = _size

    private val lastElemInd: Int
        get() = size - 1

    override fun isEmpty(): Boolean {
        return size == 0
    }

    override fun remove(): Double {
        return poll() ?: error("empty queue")
    }

    override fun contains(element: Double?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addAll(elements: Collection<Double>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun clear() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun element(): Double {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun containsAll(elements: Collection<Double>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun iterator(): MutableIterator<Double> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun remove(element: Double?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeAll(elements: Collection<Double>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun offer(e: Double?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retainAll(elements: Collection<Double>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun peek(): Double {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun toString() = backupArray.take(size).joinToString(prefix = "[", postfix = "]")

    companion object {

        fun parentInd(childInd: Int) = (childInd - 1) / 2
        fun leftChildInd(parentInd: Int) = parentInd * 2 + 1
        fun swap(arr: Array<Double>, li: Int, ri: Int) {
            val tmp = arr[li]
            arr[li] = arr[ri]
            arr[ri] = tmp
        }

        fun repairTop(arr: Array<Double>, topInd: Int, lastElemInd: Int) {

            fun minChildInd(): Int? {

                val leftChildInd = leftChildInd(topInd)
                if (leftChildInd > lastElemInd) return null
                val rightChildInd = leftChildInd + 1
                if (rightChildInd > lastElemInd) return leftChildInd
                return if (arr[rightChildInd] < arr[leftChildInd]) rightChildInd
                else leftChildInd
            }
            minChildInd()?.let {
                if (arr[it] < arr[topInd]) {
                    swap(arr, it, topInd)
                    repairTop(arr, it, lastElemInd)
                }
            }

        }

        fun buildHeap(arr: Array<Double>, firstElemInd: Int, lastElemInd: Int) {
            for (topInd in lastElemInd downTo firstElemInd) {
                repairTop(arr, topInd, lastElemInd)
                // println("$topInd ${arr.take(lastElemInd + 1).joinToString()}")
            }
        }

    }

}

fun Array<Double>.heapSort() {

    PriorityQueue.buildHeap(this, 0, this.lastIndex)
    for (lastInd in this.lastIndex downTo 1) {
        PriorityQueue.swap(this, 0, lastInd)
        PriorityQueue.repairTop(this, 0, lastInd - 1)
    }
}
