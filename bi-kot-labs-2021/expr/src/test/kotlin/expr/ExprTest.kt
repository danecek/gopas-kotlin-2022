package expr

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class ExprTest : StringSpec({
    "Bin plus" {
        Bin(Oper.PLS, Num(1), Num(2)).value shouldBe 3
        Bin(Oper.MNS, Num(1), Num(2)).value shouldBe -1
    }

})