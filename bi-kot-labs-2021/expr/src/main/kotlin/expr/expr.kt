package expr

import expr.Bin.Companion.enclose
import visitor.ExprVisitor
import visitor.PostfixVisitor

enum class Oper(val smb: String, val pri: Int) {
    PLS("+", 2), MNS("-", 2), MLT("*", 1);
    override fun toString() = smb
}

interface Expr {
    val value: Int
    val pri: Int
    fun accept(v: ExprVisitor)
}

data class Num(override val value: Int) : Expr {
    override val pri = 0
    override fun accept(v: ExprVisitor) {
        v.visitNum(this)
    }
    override fun toString() = value.toString()
}

val ZERO = Num(0)
val ONE = Num(1)
val TWO = Num(2)

data class Un(val op: Oper, val oprnd: Expr) : Expr {
    override val value: Int
        get() {
            val v = oprnd.value
            return if (op == Oper.MNS) -v else v
        }

    override val pri=1

    override fun accept(v: ExprVisitor) {
        v.visitUn(this)
    }
    override fun toString(): String {
        val s: String = if (oprnd.pri > pri) oprnd.enclose() else oprnd.toString()
        return "$op$s"
    }
}

data class Bin(val op: Oper, val left: Expr, val right: Expr) : Expr {
    override fun accept(v: ExprVisitor) {
        v.visitBin(this)
    }

    override val pri = op.pri
    override val value = when (op) {
        Oper.PLS -> left.value + right.value
        Oper.MNS -> left.value - right.value
        Oper.MLT -> left.value * right.value
    }

    companion object {
        fun Any.enclose() = "($this)"
    }

    override fun toString(): String {
        val l: String = if (left.pri > pri) left.enclose() else left.toString()
        val r: String = if (right.pri > pri) left.enclose() else right.toString()
        return "$l $op $r"
    }
}


fun main() {
    val e = Un(Oper.MNS,Bin(Oper.PLS, TWO, ONE))
    val e2 = Bin(Oper.MLT, e, Num(3))
    val pv = PostfixVisitor()
    e2.accept(pv)
    println("$e2= ${e2.value}")
    println("${pv.result} = ${e2.value}")
}