package dsl

import expr.Bin
import expr.Expr
import expr.Num
import expr.Oper
import java.util.*

// Lze pouzit jeden globální zasobník  ************************************************************

fun Stack<Expr>.num(v: Int): Expr = apply { push(Num(v)) }.peek()

fun Stack<Expr>.bin(op: Oper, createOperands: Stack<Expr>.() -> Unit): Expr {
    return apply { createOperands() }
        .apply { push(Bin(op, right = pop(), left = pop())) }
        .peek()
}

fun Stack<Expr>.pls(block: Stack<Expr>.() -> Unit) = bin(Oper.PLS, block)

// Fronta musí být lokální ****************************************************************

fun Queue<Expr>.num(v: Int) = apply { add(Num(v)) }.peek()

fun Queue<Expr>.bin(op: Oper, createOperands: Queue<Expr>.() -> Unit): Expr {
    val bin = LinkedList<Expr>()
        .apply { createOperands() }
        .apply { check(size == 2) }
        .run { Bin(op, remove(), remove()) }
    return apply { add(bin) }.last()
}

fun Queue<Expr>.pls(block: Queue<Expr>.() -> Unit) = bin(Oper.PLS, block)

fun main() {
    val e = LinkedList<Expr>().pls {
        num(3)
        pls {
            num(1)
            num(2)
        }
    }
    println(e)
    val e2 = Stack<Expr>().pls {
        num(3)
        pls {
            num(1)
            num(2)
        }
    }
    println(e2)
}