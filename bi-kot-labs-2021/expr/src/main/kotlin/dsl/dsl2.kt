package dsl

import expr.Bin
import expr.Expr
import expr.Num
import expr.Oper.MLT
import expr.Oper.PLS


operator fun Expr.plus(r: Expr) = Bin(PLS, this, r)
operator fun Expr.plus(r: Int) = Bin(PLS, this, Num(r))
operator fun Expr.times(r: Expr) = Bin(MLT, this, r)
operator fun Expr.times(r: Int) = Bin(MLT, this, Num(r))

operator fun Int.not() = Num(this)

fun main() {
    println((!1 + 2) * 3)

}