package patt

import expr.*
import expr.Oper.*

interface ExprPatt {
    fun match(e: Expr): Boolean
}

open class NumPatt(private val v: Int) : ExprPatt {
    override fun match(e: Expr) = (e is Num) && e.value == v
}

open class UnPatt(private val op: Set<Oper> = setOf(PLS, MNS), private val opPatt: ExprPatt = UnivPatt) : ExprPatt {
    override fun match(e: Expr): Boolean {
        return e is Un && e.op in op && opPatt.match(e.oprnd)
    }
}

open class BinPatt(
    private val op: Set<Oper> = values().toSet(),
    private val leftPatt: ExprPatt = UnivPatt,
    val rightPatt: ExprPatt = UnivPatt
) : ExprPatt {
    override fun match(e: Expr) = (e is Bin)
            && e.op in op
            && leftPatt.match(e.left)
            && rightPatt.match(e.right)
}

object UnivPatt : ExprPatt {
    override fun match(e: Expr): Boolean = true
}

object ZeroPatt : NumPatt(0)
object OnePatt : NumPatt(1)
object UnivUnPatt : UnPatt()
object UnivBinPatt : BinPatt()
object ZeroPlsPatt : BinPatt(setOf(PLS), ZeroPatt)
object PlsZeroPatt : BinPatt(setOf(PLS), rightPatt = ZeroPatt)
object OneTimesPatt : BinPatt(setOf(MLT), OnePatt)
object TimesOnePatt : BinPatt(setOf(MLT), rightPatt = OnePatt)


object UnPlsPatt : UnPatt(setOf(PLS))
open class UnMnsPatt(oprndPatt: ExprPatt = UnivPatt) : UnPatt(setOf(MNS), oprndPatt)
object UnMnsMnsPatt : UnMnsPatt(UnMnsPatt())

fun Expr.optim(): Expr {
    return when {
        UnPlsPatt.match(this) -> (this as Un).oprnd.optim()
        UnMnsMnsPatt.match(this) -> ((this as Un).oprnd as Un).oprnd.optim()
        ZeroPlsPatt.match(this) -> (this as Bin).right.optim()
        PlsZeroPatt.match(this) -> (this as Bin).left.optim()
        OneTimesPatt.match(this) -> (this as Bin).right.optim()
        TimesOnePatt.match(this) -> (this as Bin).left.optim()
        UnivBinPatt.match(e = this) -> (this as Bin).copy(left = left.optim(), right = right.optim())
        UnivUnPatt.match(e = this) -> (this as Un).copy(oprnd = oprnd.optim())
        else -> this
    }
}

fun main() {
    listOf(
        Un(PLS, TWO),
        Un(MNS, (Un(MNS, Un(PLS, TWO)))),
        Un(MNS, (Un(PLS, Un(PLS, TWO)))),
        Bin(PLS, ZERO, TWO),
        Bin(PLS, TWO, ZERO),
        Bin(MLT, ONE, TWO),
        Bin(MLT, TWO, ONE)
    ).forEach {
        println("$it is optimized to ${it.optim()}")
    }
}

