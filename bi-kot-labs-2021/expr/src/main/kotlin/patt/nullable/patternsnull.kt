package patt.nullable

import expr.*
import expr.Oper.*

interface ExprPatt {
    fun match(e: Expr): Expr?
}

object UnivPatt : ExprPatt {
    override fun match(e: Expr) = e
}

open class NumPatt(private val v: Int) : ExprPatt {
    override fun match(e: Expr) = if (e is Num && e.value == v) e
    else null
}

object ZeroPatt : NumPatt(0)
object OnePatt : NumPatt(1)

open class UnPatt(private val ops: Array<Oper> = values(), private val oprndPatt: ExprPatt = UnivPatt) : ExprPatt {
    override fun match(e: Expr): Un? =
        if (e is Un && e.op in ops && oprndPatt.match(e.oprnd) != null) e
        else null
}

open class UnMnsPat(oprndPatt: ExprPatt = UnivPatt) : UnPatt(arrayOf(MNS), oprndPatt)

open class BinPatt(
    private val op: Array<Oper> = Oper.values(),
    private val leftPatt: ExprPatt = UnivPatt,
    val rightPatt: ExprPatt = UnivPatt
) : ExprPatt {
    override fun match(e: Expr): Bin? =
        if (e is Bin &&
            e.op in op &&
            leftPatt.match(e.left) != null &&
            rightPatt.match(e.right) != null
        ) e
        else null
}

object UnivUnPatt : UnPatt()
object UnivBinPatt : BinPatt()
object UnPlsPatt : UnPatt(arrayOf(PLS))
object UnMnsMnsPatt : UnMnsPat(UnMnsPat())

object ZeroPlsPatt : BinPatt(arrayOf(PLS), ZeroPatt)
object PlsZeroPatt : BinPatt(arrayOf(PLS), rightPatt = ZeroPatt)
object OneTimesPatt : BinPatt(arrayOf(MLT), OnePatt)
object TimesOnePatt : BinPatt(arrayOf(MLT), rightPatt = OnePatt)

fun Expr.optimNull(): Expr =
    UnPlsPatt.match(this)?.oprnd?.optimNull()
        ?: UnMnsMnsPatt.match(this)?.oprnd?.let { it as Un }?.oprnd?.optimNull()
        ?: UnivUnPatt.match(this)?.run { copy(oprnd = this.oprnd.optimNull()) }
        ?: ZeroPlsPatt.match(this)?.right?.optimNull()
        ?: PlsZeroPatt.match(this)?.left?.optimNull()
        ?: OneTimesPatt.match(this)?.right?.optimNull()
        ?: TimesOnePatt.match(this)?.left?.optimNull()
        ?: UnivUnPatt.match(this)?.run { copy(oprnd = oprnd.optimNull()) }
        ?: UnivBinPatt.match(this)?.run { copy(left = left.optimNull(), right = right.optimNull()) }
        ?: this

fun main() {
    listOf(
        Un(PLS, TWO),
        Un(MNS, (Un(MNS, Un(PLS, TWO)))),
        Un(MNS, (Un(PLS, Un(PLS, TWO)))),
        Bin(PLS, ZERO, TWO),
        Bin(PLS, TWO, ZERO),
        Bin(MLT, ONE, TWO),
        Bin(MLT, TWO, ONE)
    ).forEach {
        println("$it is optimized to ${it.optimNull()}")
    }
}