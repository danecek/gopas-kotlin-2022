package visitor

import expr.*

interface ExprVisitor {
    fun visitNum(n: Num)
    fun visitUn(u: Un)
    fun visitBin(b: Bin)
}

class PostfixVisitor : ExprVisitor {
    val result = StringBuilder()
    override fun visitNum(n: Num) {
        result.append(' ').append(n.toString())
    }

    override fun visitUn(u: Un) {
        u.oprnd.accept(this)
        result.append(' ').append(u.op)
    }

    override fun visitBin(b: Bin) {
        b.left.accept(this)
        b.right.accept(this)
        result.append(' ').append(b.op)
    }

}

class EvalVisitor : ExprVisitor {
    var result: Int = 0
    override fun visitNum(n: Num) {
        result = n.value
    }

    override fun visitUn(u: Un) {
        u.oprnd.accept(this)
        result = when (u.op) {
            Oper.PLS -> result
            Oper.MNS -> -result
            else -> error("invalid un operation")
        }
    }

    override fun visitBin(b: Bin) {
        b.left.accept(this)
        val leftResult = result
        b.right.accept(this)
        result = when (b.op) {
            Oper.PLS -> leftResult + result
            Oper.MNS -> leftResult - result
            Oper.MLT -> leftResult * result
        }
    }

}

fun main() {
    val e = Un(Oper.MNS, Bin(Oper.PLS, TWO, ONE))
    val e2 = Bin(Oper.MLT, e, Num(3))
    val pv = EvalVisitor()
    e2.accept(pv)
    println("$e2 = ${pv.result} = ${e2.value}")
}
