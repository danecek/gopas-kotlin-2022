package fit.bikot.coroutineprimes

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeoutOrNull

fun CoroutineScope.natFrom2(): ReceiveChannel<Int> =
    produce {
        var c = 2
        while (true)
            send(c++);
    }

fun CoroutineScope.filtern(n: Int, ic: ReceiveChannel<Int>): ReceiveChannel<Int> =
    produce {
        while (true) {
            val x = ic.receive();
            if (x % n != 0)
                send(x)
        }
    }

fun CoroutineScope.prime(): ReceiveChannel<Int> =
    produce {
        send(2)
        var fn: ReceiveChannel<Int> = filtern(2, natFrom2())
        while (true) {
            val e: Int = fn.receive();
            fn = filtern(e, fn)
            send(e)
        }
    }

suspend fun maxPrime(timeOut: Int): Int {
    var maxp = 0
    return withTimeoutOrNull(3000) {
        prime().consumeEach {
            maxp = it
        } as Nothing
    } ?: maxp

}

fun main() {
    runBlocking {
        val iter = prime().iterator()
        (1..20).forEach {
            check(iter.hasNext())
            println(iter.next())
        }
        println(maxPrime(3000))
    }
}

