package kotlin2021.exposed

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

data class Customer(val id: Int, val name: String, val receipts: List<Receipt> = listOf<Receipt>()){
    override fun toString()="Customer($id, $name, $receipts)"
}

data class Receipt(val id: Int, val description: String, val custId: Int) {
    override fun toString()="Receipt($id, $description, custId=$custId)"
}

object CustomerTable : IntIdTable() {
    val name = varchar("name", 50)
}

object ReceiptTable : IntIdTable() {
    val description = varchar("name", 500)
    val custId = reference("cust_id", CustomerTable)
}

object CustomerExposed {
    init {
        Database.connect(
            "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1",
            driver = "org.h2.Driver"
        )
        transaction {
            SchemaUtils.create(CustomerTable, ReceiptTable)
        }
    }

    fun searchReceipts(templ: String): List<Receipt> =
        transaction {
            ReceiptTable.select { ReceiptTable.description like templ }.map {
                Receipt(it[ReceiptTable.id].value, it[ReceiptTable.description], it[ReceiptTable.custId].value)
            }
        }

    fun check(): Unit =
        transaction {
            ReceiptTable.slice(ReceiptTable.description).selectAll().map {
                check(it[ReceiptTable.description].isNotBlank())
            }
        }

    fun allCustomers(): List<Customer> = transaction {
            val custId2rcpts: Map<Int, List<Receipt>> = allReceipts().groupBy { it.custId }
            CustomerTable.selectAll().map {
                val custId = it[CustomerTable.id].value
                Customer(custId, it[CustomerTable.name], custId2rcpts[custId] ?: mutableListOf())
            }
        }


    fun createCust(name: String): Int =
        transaction {
            CustomerTable.insertAndGetId {
                it[CustomerTable.name] = name
            }.value
        }

    fun createReceipt(custId: Int, description: String): Int =
        transaction {
            ReceiptTable.insertAndGetId {
                it[ReceiptTable.description] = description
                it[ReceiptTable.custId] = custId
            }.value
        }

    fun allReceipts(): List<Receipt> = transaction {
        ReceiptTable.selectAll().map {
            Receipt(it[ReceiptTable.id].value, it[ReceiptTable.description], it[ReceiptTable.custId].value)
        }
    }

    fun updateReceipt(id: Int, description: String) {
        transaction {
            ReceiptTable.update({ ReceiptTable.id eq id }) {
                it[ReceiptTable.description] = description
            }
        }
    }

    fun deleteReceipt(id: Int) {
        transaction {
            ReceiptTable.deleteWhere { ReceiptTable.id eq id }
        }
    }

    fun receiptsByCustomer(custId: Int): List<Receipt> =
        transaction {
            ReceiptTable.select { ReceiptTable.custId eq custId }.map {
                Receipt(it[ReceiptTable.id].value, it[ReceiptTable.description], it[ReceiptTable.custId].value)
            }
        }

    fun customerById(id: Int): Customer =
        transaction {
            CustomerTable.select { CustomerTable.id eq id }.map {
                Customer(id, it[CustomerTable.name], receiptsByCustomer(id))
            }.single()
        }
}


fun main() {

    with(CustomerExposed) {
        val tomId = createCust("Tom")
        val bobId = createCust("Bob")
        println(customerById(tomId))
        val rcpt1Id = createReceipt(tomId, "Tom's 1")
        createReceipt(tomId, "Tom's 2")
        updateReceipt(rcpt1Id, "changed Tom's")
        println(allReceipts())
        println(customerById(tomId))
        createReceipt(bobId, "Bob's")
        println(allCustomers())
        println(allReceipts())
        deleteReceipt(1)
        println(receiptsByCustomer(tomId))
    }
}