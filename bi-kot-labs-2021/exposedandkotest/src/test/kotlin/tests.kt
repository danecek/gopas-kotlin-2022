import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.Matcher
import io.kotest.matchers.MatcherResult
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.should
import kotlin2021.exposed.CustomerExposed
import kotlin2021.exposed.CustomerExposed.createReceipt
import kotlin2021.exposed.CustomerExposed.customerById

fun nameMatcher() = object : Matcher<String> {
    override fun test(value: String): MatcherResult {
        return MatcherResult(value.isNotBlank() && value[0].isUpperCase(),
            "should start by capital", "should not start by capital")
    }
}

fun String.shouldBeName() = this should nameMatcher()


class PropertyExample : StringSpec({


    val tomId = CustomerExposed.createCust("tom")
    val rcptDscs = listOf("tomsreceipt1", "tomsreceipt2")
    "cust test" {
        customerById(tomId).name.shouldBeName()
    }
    "receipt test" {
        createReceipt(tomId, rcptDscs[0])
        createReceipt(tomId, rcptDscs[1])
        customerById(tomId).receipts.map { it.description } shouldContainAll rcptDscs
    }
})