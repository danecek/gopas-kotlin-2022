package gopas.kotlin.seq

class MySeq<T>(_it : Iterator<T>) : Sequence<T> {
    val it : Iterator<T> = _it
    override fun iterator(): Iterator<T> = it;
}

fun <T, R> MySeq<T>.eagerMap(m: (T) -> R): MySeq<R> {
    val mutableList = mutableListOf<R>()
    for (e in this) {
        mutableList.add(m(e))
    }
    return MySeq(mutableList.iterator())
}

fun <T, R> MySeq<T>.lazyMap(m: (T) -> R): MySeq<R> {

    return MySeq(object : Iterator<R>{
        override fun hasNext(): Boolean {
           return it.hasNext()
        }

        override fun next(): R {
            return m(it.next())
        }
    })
}

fun main(args: Array<String>) {
    val ms: MySeq<String> = MySeq(listOf("abc", "xyz").iterator());
    val fms = ms.lazyMap { it.toUpperCase() }
   for (x in  fms) {
       println(x)
   }
}