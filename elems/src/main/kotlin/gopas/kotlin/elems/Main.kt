package gopas.kotlin.elems

interface Elem {
    val content: Array<String>
    val width: Int
        get() = content[0].length
    val height: Int
        get() = content.size

    infix fun above(e: Elem) {
        check(width == e.width) { "nesouhlas sirky" }
        BasicElem(content + e.content)
    }
    infix fun beside(e: Elem): Elem {
        check(height == e.height) { "nesouhlas vysky" }
        val nc = Array(e.height) { ind ->
            content[ind] + e.content[ind]
        }
        return BasicElem(nc)
    }
}

class BasicElem(override val content: Array<String>) : Elem {
    init {
        check(content.size != 0)
        val w = content[0].length
        for (l in content) {
            check(w == l.length) {" bla "}
        }
    }

    override fun toString() = content.joinToString(separator = "\n")
}

fun main(args: Array<String>) {
    val e1 = BasicElem(arrayOf("xxxx", "yyyy"))
    val e2 = BasicElem(arrayOf("aaa", "bbb"))
    println(e1 above e2)
    println(e1 beside  e2)
}