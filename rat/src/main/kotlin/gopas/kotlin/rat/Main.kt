package gopas.kotlin.rat

data class Rat(val nom: Int, val den: Int) {
    override fun toString() = "$nom/$den"
    operator fun times(that: Rat) =
        Rat(nom * that.nom, den * that.den)

    operator fun times(that: Int) =
        Rat(nom * that, den)

    fun inv() = Rat(den, nom)
    operator fun div(that: Int) =
        this / !that

    operator fun div(that: Rat) =
        this * that.inv()

    fun norm(): Rat {
        val gcd = gcd(nom, den)
        return Rat(nom / gcd, den / gcd)
    }

    companion object {
        val One = Rat(1, 1)
        fun gcd(n: Int, d: Int): Int =
            if (d == 0) n
            else gcd(d, n % d)

    }

    override fun equals(that: Any?): Boolean {
        if (this === that) return true
        if (that !is Rat) return false
        val thisn = norm()
        val thatn = that.norm()
        return thisn.nom == thatn.nom && thisn.den == thatn.den
    }

}

operator fun Int.not() = Rat(this, 1)

fun main(args: Array<String>) {
    val r1 = Rat(1, 2)
    val r2 = Rat(2, 4)
    println("$r2 === ${r2.norm()}")
    println(r1 * r1)
    println(r1 * 2)
    println(r1 == r2)
    println(!2 / 3)
    val r3: Rat = !2/3
    println(r3)

}